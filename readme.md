# Python Text Obfuscator
A small program to make reading text in open source code a little harder.

## Requirements
Requires the ```random``` module, which should come with your Python installation.

## Interactive usage (for obfuscating text before usage)
In a live Python environment, import the module with ```import obfuscator```.

To manually specify the values used for obfuscation, use ```obfuscator.interObfuscate(phrase,push1,push2,push2Interval,push3,push3Interval)```.

This will print your obfuscated string along with the command to deobfuscate it.

To randomly generate the values used for obfuscation, use ```obfuscator.randomInterObfuscate(phrase)```.

## Using in a program
To obfuscate a string, use ```obfuscator.obfuscate(phrase,push1,push2,push2Interval,push3,push3Interval)```.

This will return *just the obfuscated string*. You are responsible for keeping track of the values used for obfuscation.

To obfuscate a string with random values, use ```obfuscator.randObfuscate(phrase)```.

To deobfuscate a string, use ```obfuscator.deobfuscate(phrase,push1,push2,push2Interval,push3,push3Interval)```.

## Why?

### DO NOT USE THIS FOR SECURITY. THIS IS NOT MEANT FOR SECURITY. EVEN IF YOU HIDE YOUR OBFUSCATION NUMBERS, THIS IS NOT SECURITY.

I made this to hide an easter egg in [The Random Bot](https://gitlab.com/xarvatium/the-random-project). While it won't prevent a dedicated person from finding out what it says, it should prevent someone doing a quick readthrough of the source code and discovering the easter egg.