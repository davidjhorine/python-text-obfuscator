from random import randint

def obfuscate(phrase,push1,push2,push2Interval,push3,push3Interval):
    numChars = []
    for char in list(phrase):
        numChars.append(ord(char) + push1)
    iteration = 0
    for char in numChars:
        if (iteration % push2Interval) == 0:
            char += push2
        if (iteration % push3Interval) == 0:
            char += push3
        if iteration == 0:
            obfString = str(char)
        else:
            obfString += "/" + str(char)
        iteration += 1
    return(obfString)

def interObfuscate(phrase,push1,push2,push2Interval,push3,push3Interval):
    obfString = obfuscate(phrase,push1,push2,push2Interval,push3,push3Interval)
    print("Your obfuscated string: ")
    print(obfString)
    print("To deobfuscate:")
    print("deobfuscate(\"" + obfString + "\"," + str(push1) + "," + str(push2) + "," + str(push2Interval) + "," + str(push3) + "," + str(push3Interval) + ")")

def deobfuscate(phrase,push1,push2,push2Interval,push3,push3Interval):
    iteration = 0
    deMathed = []
    clearString = ""
    for char in phrase.split('/'):
        char = int(char) - push1
        if (iteration % push2Interval) == 0:
            char -= push2
        if (iteration % push3Interval) == 0:
            char -= push3
        deMathed.append(char)
        iteration += 1
    for char in deMathed:
        clearString += chr(int(char))
    return(clearString)

def randObfuscate(phrase):
    return obfuscate(
        phrase,
        randint(0,256),
        randint(0,256),
        randint(1,5),
        randint(0,256),
        randint(1,5)
    )

def randInterObfuscate(phrase,push1,push2,push2Interval,push3,push3Interval):
    obfString = randObfuscate(phrase)
    print("Your obfuscated string: ")
    print(obfString)
    print("To deobfuscate:")
    print("deobfuscate(\"" + obfString + "\"," + str(push1) + "," + str(push2) + "," + str(push2Interval) + "," + str(push3) + "," + str(push3Interval) + ")")
